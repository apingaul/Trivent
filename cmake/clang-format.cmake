# -- CMake script to find all source files and setup clang-format targets for them
# -- Heavily inspired from https://gitlab.cern.ch/allpix-squared/allpix-squared/blob/master/cmake/clang-cpp-checks.cmake

# -- Adding clang-format target if executable is found
# -- Will look for a .clang-format file at the root of the project
message(STATUS "Looking for clang-format")
find_program(CLANG_FORMAT "clang-format" REQUIRED)

if(CLANG_FORMAT)
  message(STATUS "Looking for clang-format -- found ")
  add_custom_target(
    clang-format
    COMMAND clang-format -i -style=file ${ALL_CXX_SOURCE_FILES}
    COMMENT "Checking for code formatting with clang-format"
  )

  ADD_CUSTOM_TARGET(
    clang-format-check
    COMMAND
    clang-format
    -style=file
    -output-replacements-xml
    ${ALL_CXX_SOURCE_FILES}
    # print output
    | tee ${CMAKE_BINARY_DIR}/clang-format.log | grep -c "replacement " |
    tr -d "[:cntrl:]" && echo " replacements necessary"
    # WARNING: fix to stop with error if there are problems
    COMMAND ! grep -c "replacement "
    ${CMAKE_BINARY_DIR}/clang-format.log > /dev/null
    COMMENT "Checking format compliance"
)


else()
  message(STATUS "Looking for clang-format -- not found ")
  message(WARNING "clang-format not found, won't run linting")
endif()
