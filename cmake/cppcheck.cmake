# -- CMake script to create cppcheck target

message(STATUS "Looking for cppcheck")
find_program(CPPCHECK "cppcheck")
if(CPPCHECK)
    message(STATUS "Looking for cppcheck -- found")
    set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

    add_custom_target(
        cppcheck
        COMMAND ${CPPCHECK}
        --enable=all
        --suppress=missingInclude
        --suppress=unmatchedSuppression
        --suppress=unusedFunction # Needed with Marlin
        --project=${CMAKE_BINARY_DIR}/compile_commands.json
        --std=c++14
        --verbose
        --quiet
        --xml-version=2
        --language=c++
        --platform=native
        --output-file=${CMAKE_BINARY_DIR}/cppcheck_results.xml
        ${ALL_CXX_SOURCE_FILES}
        COMMAND ! grep -c "error id=" ${CMAKE_BINARY_DIR}/cppcheck_results.xml > /dev/null
        COMMENT "Scanning project with cppcheck"
    )

    message(STATUS "Looking for cppcheck-htmlreport")
    find_program(CPPCHECK_HTML "cppcheck-htmlreport")
    if(CPPCHECK_HTML)
        message(STATUS "Looking for cppcheck-htmlreport -- found")
        add_custom_target(
            cppcheck-html
            COMMAND ${CPPCHECK_HTML}
            --title=${CMAKE_PROJECT_NAME}
            --file=${CMAKE_BINARY_DIR}/cppcheck_results.xml
            --report-dir=${CMAKE_BINARY_DIR}/cppcheck_results
            --source-dir=${CMAKE_SOURCE_DIR}
            COMMENT "Convert cppcheck report to HTML output"
        )
        add_dependencies(cppcheck-html cppcheck)
    else(CPPCHECK_HTML)
        message(STATUS "Looking for cppcheck-html -- not found")
        message(WARNING "cppcheck-html not found, won't create html report")
    endif()
else(CPPCHECK)
    message(STATUS "Looking for cppcheck -- not found")
    message(WARNING "cppcheck not found, won't run static-code analysis")
endif()
