message(STATUS "Looking for clang-tidy")

# -- Adding clang-tidy target if executable is found
# -- Will look for a .clang-tidy file at the root of the project
find_program(CLANG_TIDY "clang-tidy")
if(CLANG_TIDY)
  message(STATUS "Looking for clang-tidy -- found ")
  message(STATUS "Looking for run-clang-tidy.py")
  # -- Get the real path of exec (resolve simlinks) then get its directory
  get_filename_component(CLANG_TIDY ${CLANG_TIDY} REALPATH)
  get_filename_component(CLANG_DIR ${CLANG_TIDY} DIRECTORY)
  set(RUN_CLANG_TIDY_HINTS ${CLANG_DIR}/.. /usr)

  # -- Off course osx needs to be the special kid...
  if(${CMAKE_HOST_SYSTEM_NAME} STREQUAL Darwin )
    string(REPLACE "." ";" CMAKE_CXX_COMPILER_VERSION_LIST ${CMAKE_CXX_COMPILER_VERSION})
    list(GET CMAKE_CXX_COMPILER_VERSION_LIST 0 CMAKE_CXX_COMPILER_VERSION_MAJOR)
    list(GET CMAKE_CXX_COMPILER_VERSION_LIST 1 CMAKE_CXX_COMPILER_VERSION_MINOR)
    list(GET CMAKE_CXX_COMPILER_VERSION_LIST 2 CMAKE_CXX_COMPILER_VERSION_PATCH)
    set(RUN_CLANG_TIDY_HINTS ${RUN_CLANG_TIDY_HINTS} ${CLANG_DIR}/../libexec/llvm-${CMAKE_CXX_COMPILER_VERSION_MAJOR}.${CMAKE_CXX_COMPILER_VERSION_MINOR} ${CLANG_DIR}/../libexec/llvm-${CMAKE_CXX_COMPILER_VERSION_MAJOR}.0)
  endif()

  find_program(RUN_CLANG_TIDY NAMES "run-clang-tidy.py"
              PATH_SUFFIXES /share/clang /bin
              HINTS ${RUN_CLANG_TIDY_HINTS}
  )
  if(RUN_CLANG_TIDY)
    message(STATUS "Looking for run-clang-tidy.py -- found ")
    set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

    # -- Improve speed by running on all proc
    include(ProcessorCount)
    ProcessorCount(N)
    if(N EQUAL 0)
      set(N 1)
    endif()
    add_custom_target(
      clang-tidy-check
      COMMAND ${RUN_CLANG_TIDY} -p ${CMAKE_BINARY_DIR} -j${N} -header-filter=${PROJECT_SOURCE_DIR}/EventBuilder | tee ${CMAKE_BINARY_DIR}/clang-tidy.log
      COMMAND ! grep -c ": error:" ${CMAKE_BINARY_DIR}/clang-tidy.log > /dev/null
    )
    add_custom_target(
      clang-tidy-fix
      COMMAND ${RUN_CLANG_TIDY} -p ${CMAKE_BINARY_DIR} -j${N} -header-filter=${PROJECT_SOURCE_DIR}/EventBuilder -fix -format
    )
  else()
    message(STATUS "Looking for run-clang-tidy.py -- not found ")
    message(WARNING "run-clang-tidy.py not found, won't run linting")
  endif()
else()
  message(STATUS "Looking for clang-tidy -- not found ")
  message(WARNING "clang-tidy not found, won't run linting")
endif()
